package com.test.dml.wall;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.Editable;

/**
 * Created by Dzmitry Lukashanets on 02.03.2015.
 */
public class PrefHelper {

    private PrefHelper() {
    }

    public static final String TAG_QUANTITY = "quantity";
    public static final int DEFAULT_VALUE_QUANTITY = 50;

    private static final String TAG_SPEED_MIN = "speedMin";
    private static final String TAG_SPEED_MAX = "speedMax";
    private static final float DEFAULT_VALUE_SPEED_MIN = 5f;
    private static final float DEFAULT_VALUE_SPEED_MAX = 15f;

    private static final String TAG_RADIUS_MIN = "radiusMin";
    private static final String TAG_RADIUS_MAX = "radiusMax";
    private static final int DEFAULT_VALUE_RADIUS_MIN = 4;
    private static final int DEFAULT_VALUE_RADIUS_MAX = 12;

    private static final String TAG_LIFE_TIME_MIN = "lifeTimeMin";
    private static final String TAG_LIFE_TIME_MAX = "lifeTimeMax";
    private static final int DEFAULT_VALUE_LIFE_TIME_MIN = 1;
    private static final int DEFAULT_VALUE_LIFE_TIME_MAX = 6;

    public static boolean saveAllSettings(Context context, Editable quantity, Editable minSpeed, Editable maxSpeed, Editable minRadius, Editable maxRadius, Editable minLifeTime, Editable maxLifeTime) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.putInt(TAG_QUANTITY, Integer.parseInt(quantity.toString()))
                .putFloat(TAG_SPEED_MIN, Float.parseFloat(minSpeed.toString()))
                .putFloat(TAG_SPEED_MAX, Float.parseFloat(maxSpeed.toString()))
                .putInt(TAG_RADIUS_MIN, Integer.parseInt(minRadius.toString()))
                .putInt(TAG_RADIUS_MAX, Integer.parseInt(maxRadius.toString()))
                .putInt(TAG_LIFE_TIME_MIN, Integer.parseInt(minLifeTime.toString()))
                .putInt(TAG_LIFE_TIME_MAX, Integer.parseInt(maxLifeTime.toString()));
        return editor.commit();
    }

    public static int getQuantity(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt(TAG_QUANTITY, DEFAULT_VALUE_QUANTITY);
    }

    public static boolean setQuantity(Context context, int value) {
        return PreferenceManager.getDefaultSharedPreferences(context).edit().putInt(TAG_QUANTITY, value).commit();
    }

    public static float getSpeedMin(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getFloat(TAG_SPEED_MIN, DEFAULT_VALUE_SPEED_MIN);
    }

    public static boolean setSpeedMin(Context context, float value) {
        return PreferenceManager.getDefaultSharedPreferences(context).edit().putFloat(TAG_SPEED_MIN, value).commit();
    }

    public static float getSpeedMax(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getFloat(TAG_SPEED_MAX, DEFAULT_VALUE_SPEED_MAX);
    }

    public static boolean setSpeedMax(Context context, float value) {
        return PreferenceManager.getDefaultSharedPreferences(context).edit().putFloat(TAG_SPEED_MAX, value).commit();
    }

    public static int getRadiusMax(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt(TAG_RADIUS_MAX, DEFAULT_VALUE_RADIUS_MAX);
    }

    public static boolean setRadiusMax(Context context, int value) {
        return PreferenceManager.getDefaultSharedPreferences(context).edit().putInt(TAG_RADIUS_MAX, value).commit();
    }

    public static int getRadiusMin(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt(TAG_RADIUS_MIN, DEFAULT_VALUE_RADIUS_MIN);
    }

    public static boolean setRadiusMin(Context context, int value) {
        return PreferenceManager.getDefaultSharedPreferences(context).edit().putInt(TAG_RADIUS_MIN, value).commit();
    }

    public static int getLifeTimeMax(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt(TAG_LIFE_TIME_MAX, DEFAULT_VALUE_LIFE_TIME_MAX);
    }

    public static boolean setLifeTimeMax(Context context, int value) {
        return PreferenceManager.getDefaultSharedPreferences(context).edit().putInt(TAG_LIFE_TIME_MAX, value).commit();
    }

    public static int getLifeTimeMin(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt(TAG_LIFE_TIME_MIN, DEFAULT_VALUE_LIFE_TIME_MIN);
    }

    public static boolean setLifeTimeMin(Context context, int value) {
        return PreferenceManager.getDefaultSharedPreferences(context).edit().putInt(TAG_LIFE_TIME_MIN, value).commit();
    }

    public static void handlerChangePref(SharedPreferences pref, String key) {

        switch (key) {

            case TAG_SPEED_MIN:
                Particle.mSpeedMin = pref.getFloat(key, DEFAULT_VALUE_SPEED_MIN);
                break;

            case TAG_SPEED_MAX:
                Particle.mSpeedMax = pref.getFloat(key, DEFAULT_VALUE_SPEED_MAX);
                break;

            case TAG_RADIUS_MIN:
                Particle.mRadiusMin = pref.getInt(key, DEFAULT_VALUE_RADIUS_MIN);
                break;

            case TAG_RADIUS_MAX:
                Particle.mRadiusMax = pref.getInt(key, DEFAULT_VALUE_RADIUS_MAX);
                break;

            case TAG_LIFE_TIME_MIN:
                Particle.mLifeTimeMin = pref.getInt(key, DEFAULT_VALUE_LIFE_TIME_MIN);
                break;

            case TAG_LIFE_TIME_MAX:
                Particle.mLifeTimeMax = pref.getInt(key, DEFAULT_VALUE_LIFE_TIME_MAX);
                break;
        }

    }
}
