package com.test.dml.wall;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import java.util.Random;

/**
 * Created by Dzmitry Lukashanets on 02.03.2015.
 */
public class Particle {
    private static final String TAG = "Particle";

    private Random random = new Random();
    private Paint mPaint = new Paint();

    public static volatile int mRadiusMin = PrefHelper.getRadiusMin(LiveWallApplication.getContextApp());
    public static volatile int mRadiusMax = PrefHelper.getRadiusMax(LiveWallApplication.getContextApp());

    public static volatile float mSpeedMin = PrefHelper.getSpeedMin(LiveWallApplication.getContextApp());
    public static volatile float mSpeedMax = PrefHelper.getSpeedMax(LiveWallApplication.getContextApp());

    public static volatile int mLifeTimeMin = PrefHelper.getLifeTimeMin(LiveWallApplication.getContextApp());
    public static volatile int mLifeTimeMax = PrefHelper.getLifeTimeMax(LiveWallApplication.getContextApp());

    private int mRadius;
    private float x;
    private float y;
    private float xStart;
    private float yStart;
    private double mAngel;

    private float mSpeed;
    private long mLifeTime;
    private long mBeginningLifeTime;

    public Particle(int width, int height) {
        setRandomParam(width, height);
    }

    private void setRandomParam(int width, int height) {
        int color = Color.argb(random.nextInt(206) + 50, random.nextInt(256), random.nextInt(256), random.nextInt(256));
        this.mPaint.setColor(color);
        this.xStart = (float) width / 2;
        this.yStart = (float) height / 2;
        this.mAngel = random.nextDouble() * 6.28;
        this.mRadius = random.nextInt(mRadiusMax - mRadiusMin) + mRadiusMin;
        this.mSpeed = (random.nextFloat() * (mSpeedMax - mSpeedMin) + mSpeedMin) / 1000;
        this.mLifeTime = (random.nextInt(mLifeTimeMax - mLifeTimeMin) + mLifeTimeMin) * 1000;
        this.mBeginningLifeTime = System.currentTimeMillis();
    }

    private void move() {
        float t = ((float) (System.currentTimeMillis() - mBeginningLifeTime)) / 1000;

        x = xStart + convertMeterInPxOnX(mSpeed * (float) Math.cos(mAngel) * t) - convertMeterInPxOnX(LiveWallService.getGx() * t * t / 2) / 1000;
        y = yStart - convertMeterInPxOnY(mSpeed * (float) Math.sin(mAngel) * t) + convertMeterInPxOnY(LiveWallService.getGy() * t * t / 2) / 1000;
    }

    public void onDraw(Canvas canvas) {
        move();
        canvas.drawCircle(x, y, mRadius, mPaint);
    }

    public void onReuse(Canvas canvas, int width, int height) {
        setRandomParam(width, height);
        onDraw(canvas);
    }

    public boolean isVisibleOnScreen(int width, int height) {
        return x < width + mRadius && x > 0 - mRadius && y < height + mRadius && y > 0 - mRadius;
    }

    public boolean isLiveByTime() {
        return System.currentTimeMillis() < mBeginningLifeTime + mLifeTime;
    }

    private float convertMeterInPxOnY(float m) {
        return m * 39.37f * LiveWallService.getYpxPerInch();
    }

    private float convertMeterInPxOnX(float m) {
        return m * 39.37f * LiveWallService.getXpxPerInch();
    }

}
