package com.test.dml.wall;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.service.wallpaper.WallpaperService;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.WindowManager;

/**
 * Created by Dzmitry Lukashanets on 28.02.2015.
 */
public class LiveWallService extends WallpaperService implements SensorEventListener {

    private static final String TAG = "LiveWallService";

    private SensorManager mSensorManager;
    private Sensor mAccelerometer;

    private static float mYdpi;
    private static float mXdpi;

    private volatile static float[] mGravity = new float[3];

    private long mLastUpdate;

    public static float getGx() {
        return mGravity[0];
    }

    public static float getGy() {
        return mGravity[1];
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate");

        WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);
        DisplayMetrics dm = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(dm);
        mYdpi = dm.ydpi;
        mXdpi = dm.xdpi;

        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

    public static float getYpxPerInch() {
        return mYdpi;
    }

    public static float getXpxPerInch() {
        return mXdpi;
    }

    @Override
    public Engine onCreateEngine() {
        Log.d(TAG, "onCreateEngine");

        return new LiveWallEngine();
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy");
        mSensorManager.unregisterListener(this, mAccelerometer);
        super.onDestroy();
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER && System.currentTimeMillis() - mLastUpdate > 80) {

            final float alpha = 0.8f;

            mGravity[0] = alpha * mGravity[0] + (1 - alpha) * event.values[0];
            mGravity[1] = alpha * mGravity[1] + (1 - alpha) * event.values[1];
            mGravity[2] = alpha * mGravity[2] + (1 - alpha) * event.values[2];

            mLastUpdate = System.currentTimeMillis();
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    private class LiveWallEngine extends Engine {
        private static final String TAG = "LiveWallEngine";

        private WallPainting mWallPainting;

        @Override
        public void onCreate(SurfaceHolder surfaceHolder) {
            super.onCreate(surfaceHolder);
            Log.d(TAG, "onCreate");
            mWallPainting = new WallPainting(LiveWallService.this, surfaceHolder);
        }

        @Override
        public void onDestroy() {
            Log.d(TAG, "onDestroy");
            super.onDestroy();
        }

        @Override
        public void onSurfaceCreated(SurfaceHolder holder) {
            super.onSurfaceCreated(holder);
            Log.d(TAG, "onSurfaceCreated");

            mWallPainting.setSurfaceSize(holder.getSurfaceFrame().width(), holder.getSurfaceFrame().height());
            mWallPainting.startPainting();
        }

        @Override
        public void onSurfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            super.onSurfaceChanged(holder, format, width, height);
            Log.d(TAG, "onSurfaceChanged: format = " + format + ", width = " + width + ", height = " + height);

            mWallPainting.setSurfaceSize(width, height);
        }

        @Override
        public void onSurfaceDestroyed(SurfaceHolder holder) {
            Log.d(TAG, "onSurfaceDestroyed");
            mWallPainting.stopPainting();
            super.onSurfaceDestroyed(holder);
        }

        @Override
        public void onVisibilityChanged(boolean visible) {
            super.onVisibilityChanged(visible);
            Log.d(TAG, "onVisibilityChanged");

            if (visible) {
                mWallPainting.resumePainting();
            } else {
                mWallPainting.pausePainting();
            }
        }

        @Override
        public void onOffsetsChanged(float xOffset, float yOffset, float xOffsetStep, float yOffsetStep, int xPixelOffset, int yPixelOffset) {
            super.onOffsetsChanged(xOffset, yOffset, xOffsetStep, yOffsetStep, xPixelOffset, yPixelOffset);

            Log.d(TAG, "onOffsetsChanged");
        }

        @Override
        public void onTouchEvent(MotionEvent event) {
            super.onTouchEvent(event);

            Log.d(TAG, "onTouchEvent");
        }

        @Override
        public Bundle onCommand(String action, int x, int y, int z, Bundle extras, boolean resultRequested) {
            Log.d(TAG, "onCommand");

            return super.onCommand(action, x, y, z, extras, resultRequested);
        }

    }


}
