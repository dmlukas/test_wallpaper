package com.test.dml.wall;

import android.app.Application;
import android.content.Context;

/**
 * Created by Dzmitry Lukashanets on 04.03.2015.
 */
public class LiveWallApplication extends Application {

    private static Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = getApplicationContext();
    }

    public static Context getContextApp() {
        return mContext;
    }
}
