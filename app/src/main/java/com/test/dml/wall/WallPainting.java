package com.test.dml.wall;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.view.SurfaceHolder;

import java.util.ArrayList;

/**
 * Created by Dzmitry Lukashanets on 02.03.2015.
 */
public class WallPainting extends Thread {

    private static final int TIME_UPDATE_IN_MS = 40;
    private volatile int mQuantity = PrefHelper.getQuantity(LiveWallApplication.getContextApp());

    private int mWidthSurface, mHeightSurface;

    private final SurfaceHolder mSurfaceHolder;
    private ArrayList<Particle> mParticleSystem = new ArrayList<>(mQuantity);
    private Context mContext;

    private volatile boolean mWait;
    private volatile boolean mRun;

    public WallPainting(Context context, SurfaceHolder surfaceHolder) {
        this.mSurfaceHolder = surfaceHolder;
        this.mContext = context;
    }

    public void setSurfaceSize(int width, int height) {
        mWidthSurface = width;
        mHeightSurface = height;
    }

    public void startPainting() {
        PreferenceManager.getDefaultSharedPreferences(mContext).registerOnSharedPreferenceChangeListener(mPrefListener);
        mRun = true;
        this.start();
    }

    public void stopPainting() {
        mRun = false;
        synchronized (this) {
            this.notify();
        }
        PreferenceManager.getDefaultSharedPreferences(mContext).unregisterOnSharedPreferenceChangeListener(mPrefListener);
    }

    public void pausePainting() {
        mWait = true;
        synchronized (this) {
            this.notify();
        }
    }

    public void resumePainting() {
        mWait = false;
        synchronized (this) {
            this.notify();
        }
    }

    @Override
    public void run() {

        Canvas canvas = null;
        while (mRun) {
            try {
                canvas = mSurfaceHolder.lockCanvas();
                if (canvas == null) {
                    continue;
                }
                synchronized (mSurfaceHolder) {
                    Thread.sleep(TIME_UPDATE_IN_MS);
                    if (mParticleSystem.size() < mQuantity) {
                        mParticleSystem.add(new Particle(mWidthSurface, mHeightSurface));
                    }
                    draw(canvas);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                if (canvas != null) {
                    mSurfaceHolder.unlockCanvasAndPost(canvas);
                }
            }
            if (mWait) {
                synchronized (this) {
                    try {
                        wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private void draw(Canvas canvas) {
        canvas.drawColor(Color.BLACK);
        for (Particle p : mParticleSystem) {
            if (isVisible(p)) {
                p.onDraw(canvas);
            } else {
                p.onReuse(canvas, mWidthSurface, mHeightSurface);
            }
        }
    }

    private boolean isVisible(Particle p) {
        return p.isVisibleOnScreen(mWidthSurface, mHeightSurface) && p.isLiveByTime();
    }

    public SharedPreferences.OnSharedPreferenceChangeListener mPrefListener = new SharedPreferences.OnSharedPreferenceChangeListener() {
        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

            if (key.equals(PrefHelper.TAG_QUANTITY)) {
                mQuantity = sharedPreferences.getInt(key, PrefHelper.DEFAULT_VALUE_QUANTITY);
                if (mQuantity < mParticleSystem.size()) {
                    mParticleSystem.subList(mQuantity, mParticleSystem.size()).clear();
                    mParticleSystem.trimToSize();
                }
                mParticleSystem.ensureCapacity(mQuantity);
            } else {
                PrefHelper.handlerChangePref(sharedPreferences, key);
            }
        }
    };

}
