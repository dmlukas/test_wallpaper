package com.test.dml.wall;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TabHost;
import android.widget.TextView;

/**
 * Created by Dzmitry Lukashanets on 28.02.2015.
 */
public class SettingsActivity extends Activity implements View.OnClickListener {

    private static final String TAG = "SettingsActivity";

    private static final String TAG_TAB_1 = "tab1";
    private static final String TAG_TAB_2 = "tab2";
    private EditText edtQuantity, edtSpeedMin, edtSpeedMax, edtRadiusMin, edtRadiusMax, edtLifeTimeMin, edtLifeTimeMax;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");

        setContentView(R.layout.activity_settings);
        initUI();
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy");

        super.onDestroy();
    }

    private void initUI() {
        TabHost tabHost = (TabHost) findViewById(R.id.tabHost);
        tabHost.setup();

        TabHost.TabSpec spec = tabHost.newTabSpec(TAG_TAB_1);
        spec.setIndicator(getString(R.string.particle_setup));
        spec.setContent(R.id.tab1);
        tabHost.addTab(spec);

        spec = tabHost.newTabSpec(TAG_TAB_2);
        spec.setIndicator(getString(R.string.partivle_begaviour));
        spec.setContent(R.id.tab2);
        tabHost.addTab(spec);

        tabHost.setCurrentTabByTag(TAG_TAB_1);

        edtQuantity = (EditText) findViewById(R.id.edt_quantity);
        edtQuantity.setText(String.valueOf(PrefHelper.getQuantity(this)));
        findViewById(R.id.btn_apply).setOnClickListener(this);

        View view = findViewById(R.id.view_speed);
        TextView tvTitle = (TextView) view.findViewById(R.id.tv_title);
        tvTitle.setText(R.string.partivle_speed);
        edtSpeedMin = (EditText) view.findViewById(R.id.edt_min_value);
        edtSpeedMin.setText(String.valueOf(PrefHelper.getSpeedMin(this)));
        edtSpeedMax = (EditText) view.findViewById(R.id.edt_max_value);
        edtSpeedMax.setText(String.valueOf(PrefHelper.getSpeedMax(this)));

        view = findViewById(R.id.view_radius);
        tvTitle = (TextView) view.findViewById(R.id.tv_title);
        tvTitle.setText(R.string.particle_size);
        edtRadiusMin = (EditText) view.findViewById(R.id.edt_min_value);
        edtRadiusMin.setText(String.valueOf(PrefHelper.getRadiusMin(this)));
        edtRadiusMax = (EditText) view.findViewById(R.id.edt_max_value);
        edtRadiusMax.setText(String.valueOf(PrefHelper.getRadiusMax(this)));

        view = findViewById(R.id.view_lifetime);
        tvTitle = (TextView) view.findViewById(R.id.tv_title);
        tvTitle.setText(R.string.partivle_lifetime);
        edtLifeTimeMin = (EditText) view.findViewById(R.id.edt_min_value);
        edtLifeTimeMin.setText(String.valueOf(PrefHelper.getLifeTimeMin(this)));
        edtLifeTimeMax = (EditText) view.findViewById(R.id.edt_max_value);
        edtLifeTimeMax.setText(String.valueOf(PrefHelper.getLifeTimeMax(this)));
    }

    private boolean saveSettings() {
        return PrefHelper.saveAllSettings(this, edtQuantity.getText(), edtSpeedMin.getText(), edtSpeedMax.getText(), edtRadiusMin.getText(), edtRadiusMax.getText(), edtLifeTimeMin.getText(), edtLifeTimeMax.getText());
    }

    @Override
    public void onBackPressed() {
        if (checkCorrectSettings()) {
            saveSettings();
            super.onBackPressed();
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_apply) {
            onBackPressed();
        }
    }

    private boolean checkCorrectSettings() {
        if (getValueFromEdt(edtSpeedMin) >= getValueFromEdt(edtSpeedMax)) {
            edtSpeedMax.setError("Error");
            return false;
        } else {
        }

        if (getValueFromEdt(edtLifeTimeMin) >= getValueFromEdt(edtLifeTimeMax)) {
            edtLifeTimeMax.setError("Error");
            return false;
        }

        if (getValueFromEdt(edtRadiusMin) >= getValueFromEdt(edtRadiusMax)) {
            edtRadiusMax.setError("Error");
            return false;
        }

        return true;
    }

    private float getValueFromEdt(EditText edt) {
        return Float.parseFloat(edt.getText().toString());
    }
}
